#pragma once
#ifndef VERTICALTABLEMODEL_H
#define VERTICALTABLEMODEL_H
class MainWindow;
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QAbstractListModel>
#include <QDebug>
#include <QErrorMessage>
#include "mainwindow.h"

class VerticalTableModel : public QAbstractTableModel {
    Q_OBJECT
public:
    VerticalTableModel(QSqlDatabase &d, QString table, QStringList col,
                       QStringList names, MainWindow *mainWindow,
                       QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    void download();
    Qt::ItemFlags flags (const QModelIndex &index) const;
    QStringList columnsName;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QList<QList<QVariant> > matrix;
private:
    QSqlDatabase &db;
    QString tableName;
    QStringList columnsShowedName;
    int rowCountInt;
    MainWindow *main;
signals:
    void modifiedCell();
};

#endif // VERTICALTABLEMODEL_H
