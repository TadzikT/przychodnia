#pragma once
#include "VerticalTableModel.h"
#include <QSqlError>
#include <string>

VerticalTableModel::VerticalTableModel(QSqlDatabase &d, QString table,
                                       QStringList col, QStringList names,
                                       MainWindow *mainWindow,
                                       QObject *parent) :
    QAbstractTableModel(parent), db(d), tableName(table), columnsName(col),
    columnsShowedName(names), main(mainWindow) {}

void VerticalTableModel::download() {
    if (columnsName.size() == columnsShowedName.size()){
        matrix.clear();
        QSqlQuery q(db);
        q.setForwardOnly(true);
        if (!q.exec("SELECT " + columnsName.join(',') + " FROM " + tableName))
            qDebug() << q.lastError();
        QList<QVariant> row;
        for (int i = 0; i < columnsName.size(); i++)
            row.append(columnsShowedName.at(i));
        matrix.append(row);
        row.clear();
        while (q.next()) {
            for (int i = 0; i < columnsName.size(); i++)
                row.append(q.value(i).toString());
            matrix.append(row);
            row.clear();
        }
        if (!q.exec("SELECT COUNT(*) FROM " + tableName))
            qDebug() << q.lastError();
        q.next();
        rowCountInt = q.value(0).toInt();
    }
    else qDebug() << "Liczba etykiet kolumn nieprawidłowa.";
}

int VerticalTableModel::rowCount(const QModelIndex &parent) const {
    return rowCountInt;
}

int VerticalTableModel::columnCount(const QModelIndex &parent) const {
    return columnsName.size();
}

QVariant VerticalTableModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();
    if (role == Qt::TextAlignmentRole) {
        return int(Qt::AlignLeft | Qt::AlignVCenter);
    }
    else if (role == Qt::DisplayRole) {
        if (matrix.size() != 0) {
            QVariant defaulted = matrix.at(index.row()+1).at(index.column());
            if (tableName == "Wizyta" && index.column() == 1) {
                QList<QList<QVariant> > mat = main->pacjent->matrix;
                int row = -1;
                //przeszukuje od 1, bo pierwszy rzad mata to header z nazwami
                //kolumn
                for (int i = 1; i < mat.size(); i++)
                    if (mat.at(i).at(0) == defaulted) {
                        row = i;
                        break;
                    }
                QString returned = mat.at(row).at(2).toString() +
                        " (" + defaulted.toString() + ")";
                return returned;
            }
            if (tableName == "Wizyta" && index.column() == 2) {
                QList<QList<QVariant> > mat = main->lekarz->matrix;
                int row = -1;
                for (int i = 1; i < mat.size(); i++)
                    if (mat.at(i).at(0) == defaulted) {
                        row = i;
                        break;
                    }
                QString returned = mat.at(row).at(2).toString() +
                        " (" + defaulted.toString() + ")";
                return returned;
            }
            if (tableName == "Diagnoza" && index.column() == 0) {
                QList<QList<QVariant> > mat = main->wizyta->matrix;
                int row = -1;
                for (int i = 1; i < mat.size(); i++)
                    if (mat.at(i).at(0) == defaulted) {
                        row = i;
                        break;
                    }
                QString returned = mat.at(row).at(3).toString() +
                        " (" + defaulted.toString() + ")";
                return returned;
            }
            if (tableName == "Diagnoza" && index.column() == 1) {
                QList<QList<QVariant> > mat = main->choroba->matrix;
                int row = -1;
                for (int i = 1; i < mat.size(); i++)
                    if (mat.at(i).at(0) == defaulted) {
                        row = i;
                        break;
                    }
                QString returned = mat.at(row).at(2).toString() +
                        " (" + defaulted.toString() + ")";
                return returned;
            }
            if (tableName == "Choroba" && index.column() == 1) {
                QList<QList<QVariant> > mat = main->lekarstwo->matrix;
                int row = -1;
                for (int i = 1; i < mat.size(); i++)
                    if (mat.at(i).at(0) == defaulted) {
                        row = i;
                        break;
                    }
                QString returned = mat.at(row).at(1).toString() +
                        " (" + defaulted.toString() + ")";
                return returned;
            }
            if (tableName == "Interakcja" && (index.column() == 0
                                              || index.column() == 1)) {
                QList<QList<QVariant> > mat = main->lekarstwo->matrix;
                int row = -1;
                for (int i = 1; i < mat.size(); i++)
                    if (mat.at(i).at(0) == defaulted) {
                        row = i;
                        break;
                    }
                QString returned = mat.at(row).at(1).toString() +
                        " (" + defaulted.toString() + ")";
                return returned;
            }
            else return defaulted;
        }
        else qDebug() << "Najpierw zrób download.";
    }
    else if (role == Qt::EditRole) {
        return matrix.at(index.row()+1).at(index.column());
    }

    //to jest do completera, dla klasy Dialog
    else if (role == Qt::WhatsThisRole) {
        if (matrix.size() != 0)
            if (tableName == "Lekarstwo" && index.column() == 1) {
                QString returned = matrix.at(index.row()+1).at(index.column()).
                        toString() + " (" + matrix.at(index.row()+1).
                        at(index.column()-1).toString() + ")";
                return returned;
            }
            else if (tableName == "Pacjent" && index.column() == 2) {
                QString returned = matrix.at(index.row()+1).at(index.column()).
                        toString() + " (" + matrix.at(index.row()+1).
                        at(index.column()-2).toString() + ")";
                return returned;
            }
            else if (tableName == "Lekarz" && index.column() == 2) {
                QString returned = matrix.at(index.row()+1).at(index.column()).
                        toString() + " (" + matrix.at(index.row()+1).
                        at(index.column()-2).toString() + ")";
                return returned;
            }
            else if (tableName == "Wizyta" && index.column() == 3) {
                QString returned = matrix.at(index.row()+1).at(index.column()).
                        toString() + " (" + matrix.at(index.row()+1).
                        at(index.column()-3).toString() + ")";
                return returned;
            }
            else if (tableName == "Choroba" && index.column() == 2) {
                QString returned = matrix.at(index.row()+1).at(index.column()).
                        toString() + " (" + matrix.at(index.row()+1).
                        at(index.column()-2).toString() + ")";
                return returned;
            }
    }

    return QVariant();
}

QVariant VerticalTableModel::headerData(int section,
                                        Qt::Orientation orientation,
                                        int role) const {
    if (role != Qt::DisplayRole)
        return QVariant();
    if (orientation == Qt::Horizontal && matrix.size() != 0)
        return matrix.at(0).at(section);
    return QVariant();
}

Qt::ItemFlags VerticalTableModel::flags (const QModelIndex &index) const {
    //przeslij ze nie mozna edytowac komorek z kolumn z primary lub foreign ID
    if (columnsName.at(index.column()).contains("numerUbez") ||
            columnsName.at(index.column()).contains("ID"))
        return QAbstractItemModel::flags(index);
    else return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

bool VerticalTableModel::setData(const QModelIndex &index,
                                 const QVariant &value, int role) {
    if (role == Qt::EditRole) {
        QString editValue = value.toString();
        //zwroc false jesli wartosc edytowanej komorki jest taka sama jak byla
        //wczesniej
        if (editValue == matrix.at(index.row()+1).at(index.column()))
            return false;
        //dla tabel bez primary key trzeba zrobic inny update.

        //ten regex sprawdza czy QString jest liczbą. QStringi nie będące
        //liczba muszą być w pojedynczych cudzysłowach.

        static QRegExp re("\\d*");
        if (!re.exactMatch(editValue))
            editValue = "'" + editValue + "'";

        QSqlQuery q(db);
        QString query = "zle query, blad";
        if (tableName == "Interakcja" || tableName == "Diagnoza")
            query = "UPDATE " + tableName + " SET "
                + columnsName.at(index.column()) + " = " + editValue +
                " WHERE " + columnsName.at(0) + " = " +
                matrix.at(index.row()+1).at(0).toString() + " AND " +
                columnsName.at(1) + " = " +
                matrix.at(index.row()+1).at(1).toString();
        else query = "UPDATE " + tableName + " SET "
                + columnsName.at(index.column()) + " = " + editValue +
                " WHERE " + columnsName.at(0) + " = " +
                matrix.at(index.row()+1).at(0).toString();
        qDebug() << query;
        if (!q.exec(query))
            qDebug() << q.lastError();
        emit modifiedCell();
    }
    return true;
}
