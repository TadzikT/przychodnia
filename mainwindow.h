#pragma once
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
class VerticalTableModel;
#include <QMainWindow>
#include <QtWidgets>
#include <QSqlError>
#include <QSqlRelationalTableModel>
#include <QtSql>
#include <QDebug>
#include <QHash>
#include <QDialog>
#include "verticaltablemodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    VerticalTableModel *pacjent;
    VerticalTableModel *lekarz;
    VerticalTableModel *wizyta;
    VerticalTableModel *diagnoza;
    VerticalTableModel *choroba;
    VerticalTableModel *lekarstwo;
    VerticalTableModel *interakcja;
    ~MainWindow();

private slots:
    void on_tabWidget_currentChanged(int index);

    void on_addButton_released();

    void on_deleteButton_released();

private:
    Ui::MainWindow *ui;
    void showError(const QSqlError &err);
    QSqlDatabase db;
    QHash <int, VerticalTableModel *> tableModelMap;
    QHash <int, QStringList> columnNamesMap;
    QStringList lekarzHeader;
    QStringList pacjentHeader;
    QStringList wizytaHeader;
    QStringList diagnozaHeader;
    QStringList chorobaHeader;
    QStringList lekarstwoHeader;
    QStringList interakcjaHeader;
    int currentTableNameKey;
    QHash <int, QString> tableNames;
public slots:
    void updateView();
};

#endif // MAINWINDOW_H
