#include "dialog.h"
#include "ui_dialog.h"
#include <QDebug>
#include <QtSql>
#include <QCompleter>
#include <QPushButton>

#define LENGTH 6 //maksymalna ilosc labeli i lineEditów w tym dialogu

Dialog::Dialog(QString tableName, QStringList columnsNames,
               MainWindow *mainWindow, QWidget *parent)
    : QDialog(parent), ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Anuluj");
    this->tableName = tableName;
    this->columnsNames = columnsNames;
    this->setWindowTitle(this->tableName);
    labels = new QLabel*[LENGTH] {ui->label, ui->label_2, ui->label_3,
                                  ui->label_4, ui->label_5, ui->label_6};
    lineEdits = new QLineEdit*[LENGTH] {ui->lineEdit, ui->lineEdit_2,
                                        ui->lineEdit_3, ui->lineEdit_4,
                                        ui->lineEdit_5, ui->lineEdit_6};

    //ustaw nazwy labeli i pochowaj nie potrzebne, labele i lineEdity, żeby
    //było ich tyle, co kolumn w tabeli.
    for (int i = 0; i < LENGTH; i++) {
        if (i < this->columnsNames.size())
            labels[i]->setText(this->columnsNames.at(i) + ":");
        else {
            labels[i]->hide();
            lineEdits[i]->hide();
        }
    }

    //automatyczne wpisywanie kolejnych primary keyów (oprocz numeru
    //ubezpieczenia)
    int nextPrimaryID = 1;
    if (this->tableName == "Lekarz")
        nextPrimaryID = seekNextPrimaryID(mainWindow->lekarz->matrix);
    if (this->tableName == "Wizyta")
        nextPrimaryID = seekNextPrimaryID(mainWindow->wizyta->matrix);
    if (this->tableName == "Choroba")
        nextPrimaryID = seekNextPrimaryID(mainWindow->choroba->matrix);
    if (this->tableName == "Lekarstwo")
        nextPrimaryID = seekNextPrimaryID(mainWindow->lekarstwo->matrix);
    if (this->tableName == "Lekarz" || this->tableName == "Wizyta"
            || this->tableName == "Choroba" || this->tableName == "Lekarstwo") {
        labels[0]->setDisabled(1);
        lineEdits[0]->setDisabled(1);
        lineEdits[0]->setText(QString::number(nextPrimaryID));
    }

    //ustawienie completerów
    if (this->tableName == "Choroba") {
        QCompleter *completer = new QCompleter(this);
        completer->setModel(mainWindow->lekarstwo);
        completer->setCompletionColumn(1);
        completer->setCompletionRole(5);
        completer->setCaseSensitivity(Qt::CaseInsensitive);
        ui->lineEdit_2->setCompleter(completer);
    }
    if (this->tableName == "Interakcja") {
        QCompleter *completer = new QCompleter(this);
        completer->setModel(mainWindow->lekarstwo);
        completer->setCompletionColumn(1);
        completer->setCompletionRole(5);
        completer->setCaseSensitivity(Qt::CaseInsensitive);
        ui->lineEdit->setCompleter(completer);
        ui->lineEdit_2->setCompleter(completer);
    }
    if (this->tableName == "Wizyta") {
        QCompleter *completerPacjent = new QCompleter(this);
        completerPacjent->setModel(mainWindow->pacjent);
        completerPacjent->setCompletionColumn(2);
        completerPacjent->setCompletionRole(5);
        completerPacjent->setCaseSensitivity(Qt::CaseInsensitive);
        ui->lineEdit_2->setCompleter(completerPacjent);

        QCompleter *completerLekarz = new QCompleter(this);
        completerLekarz->setModel(mainWindow->lekarz);
        completerLekarz->setCompletionColumn(2);
        completerLekarz->setCompletionRole(5);
        completerLekarz->setCaseSensitivity(Qt::CaseInsensitive);
        ui->lineEdit_3->setCompleter(completerLekarz);
    }
    if (this->tableName == "Diagnoza") {
        QCompleter *completerWizyta = new QCompleter(this);
        completerWizyta->setModel(mainWindow->wizyta);
        completerWizyta->setCompletionColumn(3);
        completerWizyta->setCompletionRole(5);
        completerWizyta->setCaseSensitivity(Qt::CaseInsensitive);
        ui->lineEdit->setCompleter(completerWizyta);

        QCompleter *completerChoroba = new QCompleter(this);
        completerChoroba->setModel(mainWindow->choroba);
        completerChoroba->setCompletionColumn(2);
        completerChoroba->setCompletionRole(5);
        completerChoroba->setCaseSensitivity(Qt::CaseInsensitive);
        ui->lineEdit_2->setCompleter(completerChoroba);
    }

    //zeby focus był na pierwszym lineEdicie. bez tego focus jest na buttonie OK
    if (this->lineEdits[0]->isEnabled())
        this->lineEdits[0]->setFocus();
    else this->lineEdits[1]->setFocus();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_buttonBox_accepted()
{
    QStringList values;
    //ten regex sprawdza czy QString jest liczbą. QStringi nie będące liczbą
    //muszą być w pojedynczych cudzysłowach, co dodaje poniższy else.
    static QRegExp re("\\d*");
    static QRegExp re2(".*\\((\\d*)\\)");
    for (int i = 0; i < LENGTH; i++) {
        if (!lineEdits[i]->isHidden()) {
            //jeśli jest liczbą
            if (re.exactMatch(lineEdits[i]->text()))
                values.append(lineEdits[i]->text());
            //jeśli to jest completion czyli text z liczbą w nawiasie, i trzeba
            //pobrać tą liczbe w nawiasie
            else if (re2.exactMatch(lineEdits[i]->text()))
                values.append(re2.cap(1));
            //jeśli jest textem
            else values.append("'" + lineEdits[i]->text() + "'");
        }
    }
    QString query = "INSERT INTO " + tableName + " VALUES (" +
            values.join(',') + ")";
    qDebug() << query;
    QSqlQuery q;
    if (!q.exec(query))
        qDebug() << q.lastError();
}

int Dialog::seekNextPrimaryID(QList<QList<QVariant> > mat) {
    int highestID = 1;
    //szuka od int i = 1, bo 0 to header tabeli, z nazwami kolumn
    for (int i = 1; i < mat.size(); i++)
        if (mat.at(i).at(0).toInt() > highestID)
            highestID = mat.at(i).at(0).toInt();
    return highestID + 1;
}
