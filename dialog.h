#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include "mainwindow.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QString tableName, QStringList columnsNames,
                    MainWindow *mainWindow, QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Dialog *ui;
    QLabel** labels;
    QLineEdit** lineEdits;
    QString tableName;
    QStringList columnsNames;
    int seekNextPrimaryID(QList<QList<QVariant> > mat);
};

#endif // DIALOG_H
