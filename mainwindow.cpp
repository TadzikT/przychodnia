#pragma once
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "initdatabase.h"
#include "dialog.h"
#include "verticaltablemodel.h"
#include <QSqlQuery>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Przychodnia");
    //Inicjalizacja baza danych.
    QSqlError err = initDatabase(db);
    if (err.type() != QSqlError::NoError) {
        showError(err);
        return;
    }

    /*
    //Stworz model danych.
    model = new QSqlRelationalTableModel(ui->tableView);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->setTable("Pacjent");

    //Zapeln model wpisami.
    if (!model->select()) {
        showError(model->lastError());
        return;
    }
    //Ustaw model.
    ui->tableView->setModel(model);
    ui->tableView->show();
    */
    tableNames[0] = "Pacjent";
    tableNames[1] = "Lekarz";
    tableNames[2] = "Wizyta";
    tableNames[3] = "Diagnoza";
    tableNames[4] = "Choroba";
    tableNames[5] = "Lekarstwo";
    tableNames[6] = "Interakcja";

    pacjentHeader << "numerUbezpieczenia" << "imie" << "nazwisko" << "adres"
                                             << "telefon";
    pacjent = new VerticalTableModel(db, "Pacjent",
                                     pacjentHeader, pacjentHeader, this);
    pacjent->download();
    lekarzHeader << "lekarzID" << "imie" << "nazwisko" << "telefon";
    lekarz = new VerticalTableModel(db, "Lekarz", lekarzHeader, lekarzHeader,
                                    this);
    lekarz->download();
    wizytaHeader << "wizytaID" << "numerUbezpieczenia"
                 << "lekarzID" << "dataWizyty" << "notatkiDlaLekarza";
    wizyta = new VerticalTableModel(db, "Wizyta", wizytaHeader, wizytaHeader,
                                    this);
    wizyta->download();
    diagnozaHeader << "wizytaID" << "chorobaID" << "notatkiDlaPacjenta";
    diagnoza = new VerticalTableModel(db, "Diagnoza",
                                      diagnozaHeader, diagnozaHeader, this);
    diagnoza->download();
    chorobaHeader << "chorobaID" << "lekarstwoID" << "nazwa" << "opis";
    choroba = new VerticalTableModel(db, "Choroba",
                                      chorobaHeader, chorobaHeader, this);
    choroba->download();
    lekarstwoHeader << "lekarstwoID" << "nazwa" << "opis";
    lekarstwo = new VerticalTableModel(db, "Lekarstwo",
                                      lekarstwoHeader, lekarstwoHeader, this);
    lekarstwo->download();
    interakcjaHeader << "lekarstwoID1" << "lekarstwoID2" << "opis";
    interakcja = new VerticalTableModel(db, "Interakcja",
                                      interakcjaHeader, interakcjaHeader, this);
    interakcja->download();
    tableModelMap[0] = pacjent;
    tableModelMap[1] = lekarz;
    tableModelMap[2] = wizyta;
    tableModelMap[3] = diagnoza;
    tableModelMap[4] = choroba;
    tableModelMap[5] = lekarstwo;
    tableModelMap[6] = interakcja;
    columnNamesMap[0] = pacjentHeader;
    columnNamesMap[1] = lekarzHeader;
    columnNamesMap[2] = wizytaHeader;
    columnNamesMap[3] = diagnozaHeader;
    columnNamesMap[4] = chorobaHeader;
    columnNamesMap[5] = lekarstwoHeader;
    columnNamesMap[6] = interakcjaHeader;
    currentTableNameKey = 0;
    ui->tableView->setModel(pacjent);
    for (int i = 0; i < tableModelMap.size(); i++) {
        connect(tableModelMap[i], SIGNAL(modifiedCell()), this,
                SLOT(updateView()));
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showError(const QSqlError &err) {
    QMessageBox::critical(this, "Unable to initialize Database",
                "Error initializing database: " + err.text());
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    currentTableNameKey = index;
    ui->tableView->setModel(tableModelMap[currentTableNameKey]);
}

void MainWindow::on_addButton_released()
{
    Dialog *dialog = new Dialog(tableNames[currentTableNameKey],
                                columnNamesMap[currentTableNameKey], this);
    dialog->exec();
    updateView();
}
void MainWindow::updateView() {
    tableModelMap[currentTableNameKey]->download();
    //pojawia sie wpis od razu po dodaniu wpisu,
    //bez potrzeby zmiany zakladki na inna i z powrotem, ale zamiast byc nowym
    //wpisem, to ten wpis zastępuje jeden z poprzednich wpisow w tabeli.
    //Zeby wiec to naprawic to jest taki haks ponizej.
    int tableNumber = currentTableNameKey + 1;
    if (tableNumber > 6) tableNumber = 0;
    ui->tableView->setModel(tableModelMap[tableNumber]);
    ui->tableView->setModel(tableModelMap[currentTableNameKey]);
}

void MainWindow::on_deleteButton_released()
{
    //trzeba zaznaczac cale rzedy zeby dalo sie usunac. by zaznaczyc caly rzad
    //nakliknij na ten prostakacik po lewej stronie, przed komorkami. mozna
    //pare naraz wpisow, jak sie rubber band selecta zrobi, albo z ctrl
    //przytrzymanym
    QModelIndexList list = ui->tableView->selectionModel()->selectedRows();
    for (int i = 0; i < list.count(); i++) {
        QModelIndex index = list.at(i);
        //QString id = index.data(Qt::DisplayRole).toString();
        QString id = tableModelMap[currentTableNameKey]->
                matrix.at(index.row()+1).at(0).toString();
        QSqlQuery q;
        QString query = "zle query, blad";
        if (tableModelMap[currentTableNameKey] == interakcja ||
                tableModelMap[currentTableNameKey] == diagnoza)
             query = "DELETE FROM " + tableNames[currentTableNameKey] +
                    " WHERE " + tableModelMap[currentTableNameKey]->
                     columnsName.at(0) + " = " + id + " AND " + tableModelMap
                     [currentTableNameKey]->columnsName.at(1)  + " = " +
                     tableModelMap[currentTableNameKey]->
                     matrix.at(index.row()+1).at(1).toString();
        else query = "DELETE FROM " + tableNames[currentTableNameKey] +
                " WHERE " + tableModelMap[currentTableNameKey]->
                columnsName.at(0)+ " = " + id;
        qDebug() << query;
        if (!q.exec(query))
            qDebug() << q.lastError();
    }
    updateView();
}
