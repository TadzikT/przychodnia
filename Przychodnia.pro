#-------------------------------------------------
#
# Project created by QtCreator 2016-12-05T18:56:53
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Przychodnia
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    verticaltablemodel.cpp \
    dialog.cpp

HEADERS  += mainwindow.h \
    initdatabase.h \
    verticaltablemodel.h \
    dialog.h

FORMS    += mainwindow.ui \
    dialog.ui

DISTFILES += \
    Instrukcja użytkownika.pdf \
    Opis techniczny.pdf
